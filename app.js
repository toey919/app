import React, { Component } from 'react';
import {
    StyleSheet,
    Navigator,
    Text,
    View,
    TouchableOpacity,
    Image,
    BackAndroid
} from 'react-native';

//apollo client
import ApolloClient, {createNetworkInterface} from 'apollo-client';
import { ApolloProvider } from 'react-apollo';

const networkInterface = createNetworkInterface({
    uri: "http://admin.waitlessq.com:3000/graphql"
});
const client = new ApolloClient({
    networkInterface
});

//page setting
import { MainPage } from './pages/main';
import { ProfilePage } from './pages/profile';
import { ServiceProviderInboxPage } from './pages/serviceproviderinbox';
import { ServiceProviderClientsPage } from './pages/serviceproviderclient';
import { ServiceProviderDashPage } from './pages/serviceproviderdash';
import { TicketViewPage } from './pages/ticketview';
import { SetAppointmentPage } from './pages/setappointment';
import { SetAppointmentPage2 } from './pages/setappointment2';
import { SettingPage } from './pages/setting';
import { LoginPage } from './pages/login';

import NotificationsIOS, { NotificationAction, NotificationsAndroid, NotificationCategory } from 'react-native-notifications';
NotificationsAndroid.setRegistrationTokenUpdateListener((deviceToken) => {
    console.log('Push-notifications regsitered!', deviceToken);//AIzaSyDyZdPYK5e2hCo6y3Zca-E67GRoDd_rC_M
});

NotificationsAndroid.setNotificationReceivedListener((notification) => {
    console.log("Notification received on device", notification.getData());
});
/*
let upvoteAction = new NotificationAction({
  activationMode: "background",
  title: String.fromCodePoint(0x1F44D),
  identifier: "UPVOTE_ACTION"
}, (action, completed) => {
  console.log("ACTION RECEIVED");
});

let replyAction = new NotificationAction({
  activationMode: "background",
  title: "Reply",
  behavior: "textInput",
  authenticationRequired: true,
  identifier: "REPLY_ACTION"
}, (action, completed) => {
  console.log("ACTION RECEIVED");
});

let exampleCategory = new NotificationCategory({
  identifier: "EXAMPLE_CATEGORY",
  actions: [upvoteAction, replyAction],
  context: "default"
});

NotificationsIOS.requestPermissions([exampleCategory]);
*/
var _navigation;

BackAndroid.addEventListener('hardwareBackPress', () => {
    if(_navigation && _navigation.getCurrentRoutes().length > 1){
        _navigation.pop();
        return true;
    }
});

var onSetting = function(nav){
    nav.push({
        name: "setting",
        title: "Setting Page"
    });
}

var onBack = function(){
    if(_navigation && _navigation.getCurrentRoutes().length > 1){
        _navigation.pop();
        return true;
    }
}

var onNotification = function(nav, route){
    if(route.name != "serviceproviderinbox"){
        nav.push({
            name: "serviceproviderinbox",
            title: "Service Provider Inbox"
        });
    }
}

var renderScene = function(route, navigationOperation){
    _navigation = navigationOperation;

    var page = null;
    switch(route.name){
        case "main":
           page = (
                <MainPage navigator={navigationOperation} />
            );
            break;
        case "profile":
            page = (
                <ProfilePage navigator={navigationOperation} id={route.id} type={route.type}/>
            );
            break;
        case "serviceproviderinbox":
            page = (
                <ServiceProviderInboxPage navigator={navigationOperation} />
            );
            break;
        case "serviceproviderclients":
            page = (
                <ServiceProviderClientsPage navigator={navigationOperation} />
            );
            break;
        case "serviceproviderdash":
            page = (
                <ServiceProviderDashPage navigator={navigationOperation} />
            )
            break;
        case "ticketview":
            page = (
                <TicketViewPage navigator={navigationOperation} />
            );
            break;
        case "setappointment":
            page = (
                <SetAppointmentPage  navigator={navigationOperation} id={route.id} type={route.type}/>
            );
            break;
        case "setappointment2":
            page = (
                <SetAppointmentPage2 navigator={navigationOperation} id={route.id} type={route.type} date={route.date} />
            );
            break;
        case "setting":
            page = (
                <SettingPage navigator={navigationOperation} />
            );
            break;
        case "login":
            page = (
                <LoginPage navigator={navigationOperation} />
            );
            break;
    }

    if(route.name != 'login'){
        return (
            <View style={styles.container}>
                <View style={styles.navbar}>
                    <Text style={styles.navbarTitle}>{route.title}</Text>
                    {route.name!='main'?
                    <TouchableOpacity style={styles.backBtn} onPress={() => onBack()}>
                        <Image source={require('./images/left-arrow.png')} style={styles.backIcon} />
                    </TouchableOpacity>: null}
                    {route.name!='main'?
                    <TouchableOpacity style={styles.notificationBtn} onPress={() => onNotification(navigationOperation, route)}>
                        <Image source={require('./images/alarm.png')} style={styles.notificationIcon}/>
                        <Text style={styles.notiText}>4</Text>
                    </TouchableOpacity>:null}
                    {(route.name!='main'&&route.name!='setting')?
                    <TouchableOpacity style={styles.settingBtn} onPress={() => onSetting(navigationOperation)}>
                        <Image source={require('./images/settings.png')} style={styles.settingIcon}/>
                    </TouchableOpacity>:null}
                </View>
                {page}
            </View>
        );
    }else{
        return (
            <LoginPage navigator={navigationOperation} />
        );
    }

};


export class App extends Component {
    render(){
        var initialRoute = {
            name: 'main',
            title: 'Main View'
        };

        return (
            <ApolloProvider client={client}>
                <Navigator
                    initialRoute={initialRoute}
                    style={{flex: 1}}
                    renderScene={renderScene}
                />
            </ApolloProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#30629a',
        padding: 10
    },
    navbar: {
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: '#f9f9f9',
        borderRadius: 8,
        position: 'relative'
    },
    navbarTitle: {
        color: '#30629a',
        fontSize: 18,
        textAlign: 'center',
        fontFamily: 'Roboto-Medium'
    },

    backBtn: {
        position: 'absolute',
        left: 10,
        top: 15,
        width: 25,
        height: 25
    },
    backIcon: {
        width: 25,
        height: 25
    },

    settingBtn: {
        position: 'absolute',
        right: 45,
        top: 15,
        width: 25,
        height: 25
    },

    settingIcon: {
        width: 25,
        height: 25
    },

    notificationBtn: {
        position: 'absolute',
        right: 10,
        top: 15,
        width: 25,
        height: 25
    },

    notificationIcon: {
        width: 25,
        height: 25
    },

    notiText: {
        backgroundColor: '#fadd7f',
        position: 'absolute',
        bottom: 0,
        right: 0,
        color: '#30629a',
        fontSize: 10,
        width: 15,
        height: 15,
        textAlign: 'center',
        lineHeight: 15,
        borderRadius: 8
    }
});
