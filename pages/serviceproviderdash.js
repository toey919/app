import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ListView,
    Image,
    TouchableHighlight,
    ScrollView
} from 'react-native';

class ListItem  extends Component{
    constructor(){
        super();
    }

    render(){
        return (
            <Text>Hello</Text>
        );
    }
}

export class ServiceProviderDashPage extends Component{
    constructor(){
        super();

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 != r2
        });
        this.state = {
            dataSource: ds.cloneWithRows(
                [
                    'row1',
                    'row2'
                ]
            )
        }
    }

    render(){
        return (
            <ScrollView style={styles.container}>
                <View style={styles.firstBlock}>
                    <Text style={[styles.headerText, styles.round]}>
                        Now
                    </Text>
                    <View style={styles.tickContainer}>
                        <View style={styles.tickTextContainer}>
                            <Text style={styles.font1}>John Johnson</Text>
                            <Text style={styles.font2}>Need an eye exam for drive license</Text>
                        </View>
                        <View style={styles.tickIDContainer}>
                            <View style={styles.tickIDWrapper}>                    
                                <Image source={require('../images/ticket.png')} style={styles.tickIcon} />
                                <Text style={styles.tickID}>
                                    B85673
                                </Text>               
                            </View>
                            <View style={styles.mark}></View>     
                        </View>
                        <View style={styles.tickIconContainer}>                        
                        </View>
                    </View>
                    <Text style={styles.headerText}>
                        Next
                    </Text>
                <View style={styles.tickContainer}>
                        <View style={styles.tickTextContainer}>
                            <Text style={styles.font1}>John Johnson</Text>
                            <Text style={styles.font2}>Need an eye exam for drive license</Text>
                        </View>
                        <View style={styles.tickIDContainer}>
                            <View style={styles.tickIDWrapper}>                    
                                <Image source={require('../images/ticket.png')} style={styles.tickIcon} />
                                <Text style={styles.tickID}>
                                    B85673
                                </Text>               
                            </View>
                            <View style={styles.mark}></View>     
                        </View>
                        <View style={styles.tickIconContainer}>
                            <TouchableHighlight style={styles.telBtn}>
                                <Image source={require('../images/tel.png')} style={styles.telIcon}/>
                            </TouchableHighlight>
                            <TouchableHighlight style={styles.msgBtn}>
                                <Image source={require('../images/message.png')} style={styles.msgIcon} />
                            </TouchableHighlight>                       
                        </View>
                    </View>
                </View>
                <View style={styles.secondBlock}>
                    <Text style={[styles.headerText, styles.round]}>
                        Status
                    </Text>
                    <View style={styles.statusItemContainer}>
                        <Text style={styles.statusText}>
                            Delay
                        </Text>
                        <TouchableHighlight style={styles.statusItemButton}>
                            <Text style={styles.statusBtnText}>16 Minutes</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.statusItemContainer}>
                        <Text style={styles.statusText}>
                            In Line
                        </Text>
                        <TouchableHighlight style={styles.statusItemButton}>
                            <Text style={styles.statusBtnText}>06 Clients</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.statusItemContainer}>
                        <Text style={styles.statusText}>
                            Total
                        </Text>
                        <TouchableHighlight style={styles.statusItemButton}>
                            <Text style={styles.statusBtnText}>25 Clients</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.statusItemContainer}>
                        <Text style={styles.statusText}>
                            Inbox
                        </Text>
                        <TouchableHighlight style={styles.statusItemButton}>
                            <Text style={styles.statusBtnText}>4 Messages</Text>
                        </TouchableHighlight>
                    </View>
                    <Text style={styles.footerText}>
                        Advance
                    </Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },

    firstBlock: {
        borderRadius: 8
    },  

    headerText: {
        paddingTop: 10,
        paddingBottom: 10,
        color: '#30629a',
        backgroundColor: '#9fdfcb',
        paddingLeft: 15,
        fontSize: 18,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center'
    },
    footerText: {
        paddingTop: 10,
        paddingBottom: 10,
        color: '#30629a',
        backgroundColor: '#fadd7f',
        paddingLeft: 15,
        fontSize: 18,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    tickContainer: {
        padding: 8,
        flexDirection: 'row',
        backgroundColor: '#ffffff'
    },
    font1: {
        fontSize: 14,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Thin'
    },
    font2: {
        fontSize: 12,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Thin'
    },
    tickTextContainer: {
        flex: 1
    },
    tickIDContainer: {       
        alignItems: 'center',
        justifyContent: 'center',       
        position: 'relative'
    },

    tickIDWrapper: {
        margin: 5,
        flexDirection: 'row',
        backgroundColor: '#fadd7f',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        paddingLeft: 8,
        paddingRight: 8
    },  

    tickIcon: {
        width: 20,
        height: 20,
        margin:7
    },

    tickID: {
        lineHeight: 20,
        fontSize: 18,
        color: '#30629a',
        fontFamily: 'Roboto-Black'
    },

    mark: {
        position: 'absolute',
        width: 16,
        height: 16,
        top: 0,
        right: 0,
        backgroundColor: '#ff0000',
        borderRadius: 8
    },
    tickIconContainer: {
        width: 20,
        marginLeft: 15
    }, 

    telBtn: {
        width: 20,
        height: 20
    },

    telIcon: {
        width: 20,
        height: 20
    },

    msgBtn: {
        width: 20,
        height: 20,
        marginTop: 5
    },

    msgIcon: {
        width: 20,
        height: 20
    },
    round: {
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    secondBlock: {
        marginTop: 15,
        borderRadius: 8,
        backgroundColor: '#ffffff'
    },

    statusItemContainer: {
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 15,
        paddingBottom: 15
    },

    statusText: {
        width: 200,
        fontSize: 18,
        fontFamily: 'Roboto-Light',
        color: '#30629a',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    },
    statusItemButton: {
        flex: 1,
        padding: 8,
        backgroundColor: '#b94c77',
        borderRadius: 7,
        alignItems: 'center',
        justifyContent: 'center'
    },
    statusBtnText: {
        color: 'white',
        fontFamily: 'Roboto-Light',
        fontSize: 17
    }    
});