import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ListView,
    ScrollView,
    Picker,
    Modal
}  from 'react-native';

import ModalDropDown from 'react-native-modal-dropdown';
import Communications from 'react-native-communications';
import ActionSheet from '@remobile/react-native-action-sheet';
import { ImagePickerManager } from 'NativeModules'; 
import MapView from 'react-native-maps';

export class TicketViewPage extends Component {
    constructor(props){
        super(props);

        this.state = {
            lang : "java",
            showModal: false,
            actionsheet: false,
            pic: null
        };

        this.goToMain = this.goToMain.bind(this);

        this.showMapModal = this.showMapModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.uploadImg = this.uploadImg.bind(this);    
        this.onCancel = this.onCancel.bind(this);
        this.onCapture = this.onCapture.bind(this);
        this.goToMessage = this.goToMessage.bind(this);
    }
    

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.userInfoContainer}>
                    <View style={styles.userMainInfo}>
                        <TouchableOpacity onPress={this.uploadImg}>                           
                            <Image style={styles.userImage} source={this.state.pic? {uri: this.state.pic}: require('../images/user.png')} />
                        </TouchableOpacity>
                        <Text style={styles.userName}>Dr. Abele</Text>
                    </View>
                    <View style={styles.userAddInfo}>
                        <TouchableOpacity onPress={() => Communications.phonecall('012312321', true)}>
                            <View style={styles.userAddItemContainer}>
                                <Image style={styles.icon} source={require('../images/phone.png')} />
                                <Text style={styles.iconText}>666-777-8888</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.goToMessage}>
                            <View style={styles.userAddItemContainer}>
                                <Image style={styles.icon} source={require('../images/chat.png')} />
                                <Text style={styles.iconText}>Hello@gency.com</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.showMapModal}>
                            <View style={styles.userAddItemContainer}>
                                <Image style={styles.icon} source={require('../images/location.png')} />
                                <Text style={styles.iconText}>21 jump street Olathe KS</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View> 
                <View style={styles.tickIDContainer}>
                    <Image style={styles.tickIcon} source={require('../images/ticket.png')} />
                    <Text style={styles.tickText}>
                        B85673
                    </Text>
                </View> 
                <View style={styles.appointContainer}>
                    <Text style={styles.headerText}>
                        You are 4th in line
                    </Text>
                    <Text style={styles.minsText}>
                        40
                    </Text>
                    <Text style={styles.minsDes}>
                        minutes left.
                    </Text>
                    <Text style={styles.goingByText}>
                        Going by
                    </Text>

                    <View style={styles.pickerContainer}>
                        <View style={styles.pickerSelect}>
                            <Picker
                                selectedValue={this.state.lang}
                                onValueChange={(lang) => this.setState({lang: lang})}  
                                style={{width: 160, color: '#30629a'}}                           
                            >
                                <Picker.Item label="Car" value="car"/>
                                <Picker.Item label="Foot" value="foot"/>
                                <Picker.Item label="Bicycle" value="bicycle"/>
                            </Picker>
                        </View>
                    </View>

                    <TouchableOpacity style={styles.footerBtn} onPress={this.goToMain}>
                        <Text style={styles.footerText}>
                            Cancel Appoint
                        </Text>
                    </TouchableOpacity>
                </View> 

                <Modal animationType={"slide"} transparent={false} visible={this.state.showModal} onRequestClose={()=>{}}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalNav}>
                            <Text style={styles.modalTitle}>My Location</Text>
                            <TouchableOpacity style={styles.backBtn} onPress={this.closeModal}>
                                <Image source={require('../images/cancel.png')} style={styles.backIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.modalContent}>
                            <MapView
                                initialRegion={{
                                latitude: 37.78825,
                                longitude: 22.4324,
                                latitudeDelta: 12.0922,
                                longitudeDelta: 20.0421,
                                }}

                                style={{
                                    flex: 1
                                }}

                                ref="map"
                            />
                        </View>
                    </View>
                </Modal> 

                <ActionSheet
                    visible={this.state.actionsheet}
                    onCancel={this.onCancel}
                >
                    <ActionSheet.Button onPress={this.onCapture}>Capture</ActionSheet.Button>
                    <ActionSheet.Button onPress={this.onSelectPhoto}>Photo</ActionSheet.Button>
                </ActionSheet>                        
            </View>
        );
    }

    appointment(){

    }

    getticket(){

    }    

    onSelect(){
        
    }

    goToMain(){
        this.props.navigator.push({
            name: "main",
            title: "Main View"
        });
    }

    uploadImg(){
        this.setState({
            actionsheet: true
        });
    }

    onCancel(){
        this.setState({
            actionsheet: false
        });
    }

    onCapture(){
        const options = {
	      mediaType: 'photo',
	      videoQuality: 'medium'
	    };
		ImagePickerManager.launchCamera(options, (response)  => {
		    this.setState({
		    	pic: response.uri,
		    	actionsheet: false
		    });
		});
    }

    onSelectPhoto(){
        const options = {	     
	      mediaType: 'photo',
	      videoQuality: 'medium'
	    };

		ImagePickerManager.launchImageLibrary(options, (response) => {
            this.setState({
		    	pic: response.uri,
		    	actionsheet: false
		    });
		});
    }

    goToMessage(){
        this.props.navigator.push({
            name: "serviceproviderinbox",
            title: "Service Provider Inbox"
        });
    }

    showMapModal(){
        this.setState({
            showModal: true
        })
    }

    closeModal(){
        this.setState({
            showModal: false
        })
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },

    userInfoContainer: {
        backgroundColor: '#f9dc7f',
        padding: 15,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        flexDirection: 'row'
    },

    userMainInfo: {
        width: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },

    userImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    userName: {
        fontSize: 15,
        color: '#30629a',
        textAlign: 'center',
        fontFamily: 'Roboto-Medium',
        marginTop: 5
    },

    userAddInfo: {
        flex: 1
    },

    userAddItemContainer: {
        flexDirection: 'row',
        marginBottom: 5
    },

    icon: {
        width: 20,
        height: 20
    },

    iconText: {
        marginLeft: 10,
        fontFamily: 'Roboto-Medium',
        color: '#30629a',
        lineHeight: 20,
        fontSize: 12
    },
    tickIDContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },
    tickIcon: {
        width: 30,
        height: 30,
        marginRight: 15
    },
    tickText: {
        fontFamily: "Roboto-Black",
        fontSize: 25,
        textAlign: 'center',
        color: '#30629a'
    },

    appointContainer: {
        marginTop: 15,
        backgroundColor: '#ffffff',
        borderRadius: 8,
        flex: 1
    },

    headerText: {
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center',
        fontFamily: 'Roboto-Light',
        color: '#30629a',
        backgroundColor: '#9fdfcb',
        fontSize: 18,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    footerBtn: {
        paddingTop: 7,
        paddingBottom: 7,
        backgroundColor: '#fadd7f',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8       
    },

    footerText: {
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center',
        fontFamily: 'Roboto-Light',
        color: '#30629a',
        fontSize: 18        
    },
    minsText: {
        textAlign: 'center',
        color: '#30629a',
        fontSize:  40,
        fontFamily: 'Roboto-Medium',
        marginTop: 10
    },
    minsDes: {
        textAlign: 'center',
        color: '#30629a',
        fontSize:  30,
        fontFamily: 'Roboto-Light',
        marginBottom: 10
    },

    goingByText: {
        textAlign: 'center',
        color: '#30629a',
        fontSize:  20,
        fontFamily: 'Roboto-Light'
    },

    pickerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 7,
        paddingBottom: 7,
        flex: 1
    },

    pickerSelect: {
        width: 160,
        backgroundColor: '#9fdfcb',
        borderRadius: 8
    },

    modalContainer: {
        flex: 1,
        backgroundColor: '#30629a',
        padding: 10
    },

    modalNav: {
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: '#f9f9f9',
        borderRadius: 8
    },

    modalTitle: {
        color: '#30629a',
        fontSize: 18,
        textAlign: 'center',
        fontFamily: 'Roboto-Medium'
    },

    modalContent: {
        flex: 1,
        marginTop: 15
    },
    mapView: {
        width: 200,
        height: 400
    },

    backBtn: {
        position:'absolute',
        left: 10,
        top: 15,
        width: 25,
        height: 25
    },

    backIcon: {
        width: 25,
        height: 25
    }  
});