import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    ListView
} from 'react-native';
import StarRating from 'react-native-star-rating';

//list item page
class ListItem extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <View style={styles.listItemContainer}>
                <View style={styles.listItemAvatarContainer}>
                    <Image style={styles.listItemAvatar} source={require('../images/user.png')}/>
                </View>
                <View style={styles.listItemContent}>
                    <Text style={styles.Font1}>{this.props.data.user.username}</Text>
                    <Text style={styles.Font2}>{this.props.data.comment}</Text>
                </View>
                <View style={styles.listItemRateContainer}>
                    <StarRating disabled={true} maxStars={5} rating={this.props.data.vote} starColor="#fadd7f" emptyStarColor="#fadd7f" starSize={25} selectedStar={()=>{}}/>
                </View>
            </View>
        );
    }
}
//recommend list view page
class Recommands extends  Component{
    constructor(props){
        super(props);

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 != r2
        });

        this.state = {
            votes: ds.cloneWithRows(this.props.votes)
        }
    }

    render(){
        return (
            <View style={styles.recommandContainer}>
                <Text style={styles.recommandTitle}>
                    Recommendations
                </Text>
                <ListView 
                    dataSource={this.state.votes}
                    renderRow={(rowData) => <ListItem data={rowData}/>} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    recommandContainer:{
        flex: 1,
        borderRadius: 8,
        backgroundColor: '#ffffff',
        marginTop: 15
    },

    recommandTitle: {
        textAlign: 'center',
        fontSize: 18,
        color: '#30629a',
        backgroundColor: '#9fdfcb',
        paddingTop: 10,
        paddingBottom: 10,
        fontFamily: 'Roboto-Medium',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    listItemContainer: {
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3',
        flexDirection: 'row'
    },

    listItemAvatarContainer: {
        width: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },

    listItemAvatar: {
        width: 35,
        height: 35,
        borderRadius: 20
    },

    listItemContent: {
        flex: 1
    },

    listItemRateContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },

    Font1: {
        fontSize: 18,
        color: '#505050',
        fontFamily: 'Roboto-Medium'
    },

    Font2: {
        fontFamily: 'Roboto-Thin',
        color: '#cdcdcd',
        fontSize: 14
    }

});
module.exports = Recommands;