import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    Text
}  from 'react-native';
import UserInfo from './userinfo';
import Recommands from './recommands';

//profile page Component
export class ProfilePage extends Component {
    constructor(props){
        super(props);

        this.state = {
            votes: [
                {
                    user: {
                        username: 'username1'
                    },
                    comment: 'That is sample',
                    vote: 4.5
                },
                {
                    user: {
                        username: 'username2'
                    },
                    comment: 'That is sample2',
                    vote: 3.2
                }
            ]
        }

        this.goToAppointment = this.goToAppointment.bind(this);
    }

    render(){
        return (
            <View style={styles.container}>
                <UserInfo id={this.props.id} type={this.props.type} navigator={this.props.navigator}/>
                <View style={styles.tabContainer}>
                    <TouchableOpacity style={[styles.tabItem, styles.btn1]} onPress={this.goToAppointment}>
                        <View style={styles.tabItemWrapper}>
                            <Image style={styles.tabItemIcon} source={require('../images/calendar.png')}/>
                            <Text style={styles.tabItemText}>Set Appointment</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.tabItem, styles.btn2]} onPress={this.goToTicketView}>
                        <View style={styles.tabItemWrapper}>
                            <Image style={styles.tabItemIcon} source={require('../images/ticket.png')} />
                            <Text style={styles.tabItemText}>Get a Ticket</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Recommands votes={this.state.votes}/>
            </View>
        );
    }

    goToAppointment(){
        this.props.navigator.push({
            name: "setappointment",
            title: "Set appointment",
            id: this.props.id,
            type: this.props.type
        });
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },
    tabContainer: {
        flexDirection: 'row'
    },

    tabItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#ffffff'
    },

    tabItemWrapper: {
        flexDirection: 'row'
    },

    tabItemIcon: {
        width: 40,
        height: 40
    },

    tabItemText: {
        textAlign: 'center',
        fontFamily: 'Roboto-Light',
        color: '#30629a',
        fontSize: 15,
        lineHeight: 30
    },

    btn1:{
        borderBottomLeftRadius: 8
    },

    btn2: {
        borderBottomRightRadius: 8
    }
});
