import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Switch
} from 'react-native';

export class SettingPage extends Component {
    constructor(props){
        super(props);

        this.state = {
            isAllow: true,
            isOpen: true,
            status: false
        };

        this.changeAllow = this.changeAllow.bind(this);
        this.changeOpen =  this.changeOpen.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
    }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>Set Appointment</Text>
                    <Switch
                        onValueChange={(value)=> this.setState({isAllow: value})}
                        value={this.state.isAllow}
                        style={styles.itemSwitch}
                    />
                </View>
                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>{this.state.isOpen? 'Open': 'Close'} Business</Text>
                    <Switch
                        onValueChange={(value)=> this.setState({isOpen: value})}
                        value={this.state.isOpen}
                        style={styles.itemSwitch}
                    />
                </View>
                <View style={styles.itemContainer}>
                    <Text style={styles.itemText}>Server Status</Text>
                    <View style={styles.itemColorContainer}>
                        <View style={styles.itemStatus}>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    changeAllow(){
        if(this.state.isAllow == true){
            this.setState({
                isAllow: false
            });
        }else{
            this.setState({
                isAllow: true
            });
        }
    }

    changeOpen(){
        if(this.state.isOpen == true){
            this.setState({
                isOpen: false
            });
        }else{
            this.setState({
                isOpen: true
            });
        }
    }

    changeStatus(){
        if(this.state.status == true){
            this.setState({
                status: false
            });
        }else{
            this.setState({
                status: true
            });
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },

    itemContainer: {
        flexDirection: 'row'
    },
    itemText: {
        fontFamily: 'Roboto-Light',
        color: '#ffffff'
    },

    itemSwitch: {
        flex: 1
    },

    itemColorContainer: {
        flex: 1,
        alignItems: 'flex-end',
        paddingRight: 3
    },

    itemStatus: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: '#ff0000'
    }
});