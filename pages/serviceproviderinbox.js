import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ListView
} from 'react-native';

class ListItem  extends Component{
    constructor(){
        super();
    }

    render(){
        return (
            <View style={styles.listItemContainer}>
                <Text style={styles.font1}>
                    Bret Robertson
                </Text>
                <Text style={styles.font2}>
                    Need an eye for drivers license
                </Text>
            </View>
        );
    }
}

export class ServiceProviderInboxPage extends Component{
    constructor(){
        super();

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 != r2
        });
        this.state = {
            dataSource: ds.cloneWithRows(
                [
                    'row1',
                    'row2',
                    'row3',
                    'row4'
                ]
            )
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Text style={styles.headerText}>
                    4 Messages
                </Text>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => <ListItem />}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        marginTop: 15,
        borderRadius: 8
    },

    headerText: {
        paddingTop: 10,
        paddingBottom: 10,
        color: '#ffffff',
        backgroundColor: '#9fdfcb',
        paddingLeft: 15,
        fontSize: 20,
        fontFamily: 'Roboto-Light',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },
    listItemContainer: {
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3'
    },
    font1: {
        fontSize: 16,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Thin'
    },
    font2: {
        fontSize: 14,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Thin'
    } 
});