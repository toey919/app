import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';

export class LoginPage extends Component{
    constructor(props){
        super(props);
        this.goToMainPage = this.goToMainPage.bind(this);
    }

    render(){
        return (
            <ScrollView style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={require('../images/clock.png')} style={styles.logo}/>
                </View>
                <View style={styles.formContainer}>
                    <View style={styles.formItemContainer}>
                        <View style={[styles.iconContainer, styles.emailIconContainer]}>
                            <Image source={require('../images/close-envelope.png')} style={styles.formIcon}/>
                        </View>
                        <TextInput  underlineColorAndroid="#ffffff" style={[styles.formInput, styles.emailInputContainer]} placeholder="Email Address"/>
                    </View>
                    <View style={styles.formItemContainer}>
                        <View style={styles.iconContainer}>
                            <Image source={require('../images/locked-padlock.png')} style={styles.formIcon}/>
                        </View>
                        <TextInput  secureTextEntry={true} underlineColorAndroid="#ffffff" style={styles.formInput} placeholder="Password"/>
                    </View>
                    <TouchableOpacity style={styles.loginBtn} onPress={this.goToMainPage}>
                        <Text style={styles.loginText}>Login</Text>
                    </TouchableOpacity>

                    <View style={styles.btnGroupContainer}>
                        <View style={styles.btnItemContainer}>
                            <TouchableOpacity style={styles.btn} onPress={this.goToMainPage}>
                                <Image source={require('../images/facebook.png')} style={styles.btnIcon}/>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.btnItemContainer}>
                            <TouchableOpacity style={styles.btn} onPress={this.goToMainPage}>
                                <Image source={require('../images/twiter.png')} style={styles.btnIcon}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

    goToMainPage(){
        this.props.navigator.push({
            name: "ticketview",
            title: "Ticket View"
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#30629a',
        padding: 20
    },

    logoContainer: {
        marginTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        width: 150,
        height: 150
    },

    formContainer: {
        borderRadius: 8
    },

    formItemContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#f9dc85'
    },

    iconContainer: {
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#d85525'
    },

    formIcon: {
        width: 32,
        height: 32
    },

    formInput: {
        backgroundColor: '#ffffff',
        flex: 1,
        height: 50,
        fontFamily: 'Roboto-Light',
        color: '#30629a',
        fontSize: 15,
        margin: 0
    },

    loginBtn: {
        backgroundColor: '#f4d782',
        paddingTop: 12,
        paddingBottom: 12,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },

    loginText: {
        textAlign: 'center',
        fontFamily: 'Roboto-Medium',
        color: '#30629a'
    },

    topRound: {
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    emailIconContainer: {
        borderTopLeftRadius: 8
    },

    emailInputContainer: {
        borderTopRightRadius: 8
    },

    btnGroupContainer: {
        marginTop: 20,
        flexDirection: 'row'
    },

    btnItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    btn: {
        width: 50,
        height: 50
    },

    btnIcon: {
        width: 50,
        height: 50
    }
});
