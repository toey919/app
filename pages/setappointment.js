import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ListView,
    ScrollView,
    Modal
}  from 'react-native';
import Calendar from 'react-native-calendar';
import UserInfo from './userinfo';
import Communications from 'react-native-communications';
import ActionSheet from '@remobile/react-native-action-sheet';
import { ImagePickerManager } from 'NativeModules';
import MapView from 'react-native-maps';

const customDayHeadings = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
const customMonthNames = ['January', 'February', 'March', 'April', 'May',
  'June', 'July', 'August', 'September', 'October', 'November', 'December'];

export class SetAppointmentPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            date: new Date(),
            showModal: false,
            actionsheet: false,
            pic: null
        }
        this.showMapModal = this.showMapModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.uploadImg = this.uploadImg.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onCapture = this.onCapture.bind(this);
        this.onSelectPhoto = this.onSelectPhoto.bind(this);
        this.goToMessage = this.goToMessage.bind(this);

        console.log(new Date().getTime());
    }

    render(){
        return (
            <ScrollView style={styles.container}>
                <UserInfo id={this.props.id} type={this.props.type} navigator={this.props.navigator}/>
                <View style={styles.headerContainer}>
                    <Image source={require('../images/calendar.png')} style={styles.headerIcon}/>
                    <Text style={styles.headerText}>Available Appoitments</Text>
                </View>

                <View style={styles.calendarContainer}>
                    <Calendar
                        ref="calendar"
                        events={[{date: '2016-11-21', hasEventCircle: {backgroundColor: '#9a2e2e'}}, {date: '2016-11-23', hasEventCircle: {backgroundColor: '#c93e3e'}}]}
                        scrollEnabled={false}
                        showControls
                        dayHeadings={customDayHeadings}
                        monthNames={customMonthNames}
                        titleFormat={'MMMM YYYY ...'}
                        prevButtonText={'PREV'}
                        nextButtonText={'NEXT'}
                        onDateSelect={(date) => this.goToAppointment2(this.props.navigator, date)}
                        startDate={this.state.date}
                        customStyle={{
                            title: {
                                fontFamily: 'Roboto-Medium',
                                fontSize: 25,
                                color: '#334771'
                            },
                            calendarContainer: {
                                backgroundColor: '#9edec9',
                                borderRadius: 8
                            },

                            day: {
                                color: '#000000',
                                fontFamily:'Roboto-Light',
                                fontSize: 20
                            },

                            weekendDayText: {
                                color: '#509b83',
                                fontFamily:'Roboto-Light'
                            },

                            selectedDayCircle: {
                                backgroundColor: '#c93e3e'

                            },

                            selectedDayText: {
                                color: '#ffffff',
                                fontFamily:'Roboto-Light'
                            },
                            controlButtonText: {
                                color: '#334771',
                                fontFamily:'Roboto-Light'
                            },
                            hasEventCircle: {
                                backgroundColor: '#c93e3e'
                            },

                            hasEventText: {
                                color: '#ffffff',
                                fontFamily:'Roboto-Light'
                            }
                        }}
                        />
                </View>
                <Modal animationType={"slide"} transparent={false} visible={this.state.showModal} onRequestClose={()=>{}}>
                    <View style={styles.modalContainer}>
                        <View style={styles.modalNav}>
                            <Text style={styles.modalTitle}>My Location</Text>
                            <TouchableOpacity style={styles.backBtn} onPress={this.closeModal}>
                                <Image source={require('../images/cancel.png')} style={styles.backIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.modalContent}>
                            <MapView
                                initialRegion={{
                                latitude: 37.78825,
                                longitude: 22.4324,
                                latitudeDelta: 12.0922,
                                longitudeDelta: 20.0421,
                                }}

                                style={{
                                    flex: 1
                                }}

                                ref="map"
                            />
                        </View>
                    </View>
                </Modal>

                <ActionSheet
                    visible={this.state.actionsheet}
                    onCancel={this.onCancel}
                >
                    <ActionSheet.Button onPress={this.onCapture}>Capture</ActionSheet.Button>
                    <ActionSheet.Button onPress={this.onSelectPhoto}>Photo</ActionSheet.Button>
                </ActionSheet>
            </ScrollView>
        );
    }

    goToAppointment2(nav, date){
        nav.push({
            name: "setappointment2",
            title: "Set Appointment",
            date : date
        });
    }

    uploadImg(){
        this.setState({
            actionsheet: true
        });
    }

    onCancel(){
        this.setState({
            actionsheet: false
        });
    }

    onCapture(){
        const options = {
	      mediaType: 'photo',
	      videoQuality: 'medium'
	    };
		ImagePickerManager.launchCamera(options, (response)  => {
		    this.setState({
		    	pic: response.uri,
		    	actionsheet: false
		    });
		});
    }

    onSelectPhoto(){
        const options = {
	      mediaType: 'photo',
	      videoQuality: 'medium'
	    };

		ImagePickerManager.launchImageLibrary(options, (response) => {
            this.setState({
		    	pic: response.uri,
		    	actionsheet: false
		    });
		});
    }

    goToMessage(){
        this.props.navigator.push({
            name: "serviceproviderinbox",
            title: "Service Provider Inbox"
        });
    }

    showMapModal(){
        this.setState({
            showModal: true
        })
    }

    closeModal(){
        this.setState({
            showModal: false
        })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },

    userInfoContainer: {
        backgroundColor: '#f9dc7f',
        padding: 15,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        flexDirection: 'row'
    },

    userMainInfo: {
        width: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },

    userImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    userName: {
        fontSize: 15,
        color: '#30629a',
        textAlign: 'center',
        fontFamily: 'Roboto-Medium',
        marginTop: 5
    },

    userAddInfo: {
        flex: 1
    },

    userAddItemContainer: {
        flexDirection: 'row',
        marginBottom: 5
    },

    icon: {
        width: 20,
        height: 20
    },

    iconText: {
        marginLeft: 10,
        fontFamily: 'Roboto-Medium',
        color: '#30629a',
        lineHeight: 20,
        fontSize: 12
    },

    headerContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: '#ffffff',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },

    headerIcon: {
        width: 30,
        height: 30,
        marginRight: 10
    },
    headerText: {
        lineHeight: 30,
        fontFamily: 'Roboto-Light',
        fontSize: 23,
        color: '#30629a'
    },

    calendarContainer: {
        marginTop: 15,
        borderRadius: 8,
        padding: 15,
        backgroundColor: '#9edec9'
    },
    modalContainer: {
        flex: 1,
        backgroundColor: '#30629a',
        padding: 10
    },

    modalNav: {
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: '#f9f9f9',
        borderRadius: 8
    },

    modalTitle: {
        color: '#30629a',
        fontSize: 18,
        textAlign: 'center',
        fontFamily: 'Roboto-Medium'
    },

    modalContent: {
        flex: 1,
        marginTop: 15
    },
    mapView: {
        width: 200,
        height: 400
    },

    backBtn: {
        position:'absolute',
        left: 10,
        top: 15,
        width: 25,
        height: 25
    },

    backIcon: {
        width: 25,
        height: 25
    }
});
