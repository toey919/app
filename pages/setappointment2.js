import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight,
    ListView,
    Picker,
    ScrollView,
    TouchableOpacity
}  from 'react-native';

import NotificationsIOS, { NotificationAction, NotificationsAndroid, NotificationCategory } from 'react-native-notifications';
import UserInfo from './userinfo';

const month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

export class SetAppointmentPage2 extends Component {
    constructor(props){
        super(props);

        this.state = {
            date : new Date(this.props.date),
            lang: 'java',
            time: new Array()
        }

        this.setTime = this.setTime.bind(this);

        for(var i = 0; i < 24; i++){
            this.state.time.push(
                <View style={styles.timeRowContainer}>
                    <TouchableOpacity style={styles.timeItemContainer} onPress={this.setTime}>
                        <Text style={styles.timeText}>
                            {i}: 00
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.timeItemContainer} onPress={this.setTime}>
                        <Text style={styles.timeText}>
                            {i}: 15
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.timeItemContainer} onPress={this.setTime}>
                        <Text style={styles.timeText}>
                            {i}: 30
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.timeItemContainer} onPress={this.setTime}>
                        <Text style={styles.timeText}>
                            {i}: 45
                        </Text>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <UserInfo />
                <View style={styles.headerContainer}>
                    <Image source={require('../images/calendar.png')} style={styles.headerIcon}/>
                    <Text style={styles.headerText}>Available Appoitments on {month[this.state.date.getMonth()]}  {this.state.date.getDate()}</Text>
                </View>

                <View style={styles.timeContainer}>
                    <View style={styles.selectContainer}>
                        <Picker
                            selectedValue={this.state.lang}
                            onValueChange={(lang) => this.setState({lang: lang})}
                            style={{color:'#30629a'}}
                        >
                            <Picker.Item label="Reason for meeting...." value="1"/>
                            <Picker.Item label="Reason for metting1..." value="2"/>
                        </Picker>
                    </View>
                    <ScrollView style={styles.timeListContainer}>
                        {this.state.time}
                    </ScrollView>
                </View>
            </View>
        );
    }

    setTime(){
        NotificationsAndroid.localNotification({title: "Ticket created", body: "that is sample description"});
        this.props.navigator.push({
            name: "login",
            title: "Login"
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15
    },

    userInfoContainer: {
        backgroundColor: '#f9dc7f',
        padding: 15,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        flexDirection: 'row'
    },

    userMainInfo: {
        width: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },

    userImage: {
        width: 70,
        height: 70,
        borderRadius: 35
    },

    userName: {
        fontSize: 15,
        color: '#30629a',
        textAlign: 'center',
        fontFamily: 'Roboto-Medium',
        marginTop: 5
    },

    userAddInfo: {
        flex: 1
    },

    userAddItemContainer: {
        flexDirection: 'row',
        marginBottom: 5
    },

    icon: {
        width: 20,
        height: 20
    },

    iconText: {
        marginLeft: 10,
        fontFamily: 'Roboto-Medium',
        color: '#30629a',
        lineHeight: 20,
        fontSize: 12
    },

    headerContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8
    },

    headerIcon: {
        width: 30,
        height: 30,
        marginRight: 10
    },
    headerText: {
        lineHeight: 30,
        fontFamily: 'Roboto-Light',
        fontSize: 23,
        color: '#30629a',
        width:230
    },

    timeContainer: {
        flex: 1,
        marginTop: 15,
        backgroundColor: '#30629a',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    selectContainer: {
        backgroundColor: '#9fdfcb',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    },

    timeListContainer: {
        flex: 1,
        marginTop: 3,
        backgroundColor: '#ffffff'
    },

    timeRowContainer: {
        flexDirection: 'row'
    },

    timeItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fadd7f',
        borderRadius: 8,
        margin: 8,
        paddingTop: 15,
        paddingBottom: 15
    },

    timeText: {
        fontFamily: 'Roboto-Light',
        color: '#30629a',
        textAlign: 'center'
    }

});
