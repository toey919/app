import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity
 } from 'react-native';
import Communications from 'react-native-communications';
import ActionSheet from '@remobile/react-native-action-sheet';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { ImagePickerManager } from 'NativeModules';
import MapView from 'react-native-maps';

var ViewWithData = null;

export default class UserInfo extends Component{
     constructor(props){
         super(props);

         this.state = {
             showModal: false,
             actionsheet: false,
             pic: null
         };

         this.uploadImg = this.uploadImg.bind(this);
         this.goToMessage = this.goToMessage.bind(this);
         this.showModal = this.showModal.bind(this);
         this.closeModal = this.closeModal.bind(this);
         this.onCancel = this.onCancel.bind(this);
         this.onCapture = this.onCapture.bind(this);
         this.onSelectPhoto = this.onSelectPhoto.bind(this);


         //getting data from apollo server
         let query = null;
         if(this.props.type == 'business'){
             query = gql`
                query getBusinessQuery($id : ID!){
                    getBusiness(_id: $id){
                        name,
                        owner,
                        phone,
                        email{
                            address
                        },
                        address
                    }
                }
             `;
         }else{
             query = gql`
                query getUserQuery($id: ID!){
                    getUser(_id: $id){
                        emails{
                            address
                        },
                        profile{
                            firstName,
                            lastName,
                            address,
                            avatar,
                            phone,
                            votes{
                                vote,
                                comment,
                                user{
                                    username
                                }
                            }
                        }
                    }
                }
             `;
         }

         const ViewContainer = (ret) => {

             if(this.props.type == 'business'){
                 if(ret.data.getBusiness){
                     let d = {
                         name: ret.data.getBusiness.name,
                         phone: ret.data.getBusiness.phone,
                         email: ret.data.getBusiness.email.address,
                         address: ret.data.getBusiness.address,
                         type: 'business'
                     }

                     return this.renderWithData(d);
                 }else{
                     return null;
                 }
             }else{
                 if(ret.data.getUser){
                     let d = {
                         name: ret.data.getUser.profile.firstName + " " + ret.data.getUser.profile.lastName,
                         phone: ret.data.getUser.profile.phone,
                         email: ret.data.getUser.emails[0].address,
                         address:ret.data.getUser.profile.address,
                         type: 'serviceprovider'
                     }

                     return this.renderWithData(d);
                 }else{
                     return null;
                 }
             }
         };

         ViewWithData = graphql(query, { options: { variables: { id: this.props.id } } })(ViewContainer);

     }

     render(){
         return (
             <ViewWithData/>
         );
     }

     renderWithData(d){
         return (
             <View style={styles.userInfoContainer}>
                 <View style={styles.userMainInfo}>
                     <TouchableOpacity onPress={this.uploadImg}>
                         <Image style={styles.userImage} source={this.state.pic? {uri: this.state.pic} : require('../images/user.png')} />
                     </TouchableOpacity>
                     <Text style={styles.userName}>{d.name}</Text>
                 </View>
                 <View style={styles.userAddInfo}>
                     <TouchableOpacity onPress={() => Communications.phonecall(d.phone,true)}>
                        <View style={styles.userAddItemContainer}>
                            <Image style={styles.icon} source={require('../images/phone.png')} />
                            <Text style={styles.iconText}>{d.phone}</Text>
                        </View>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.goToMessage}>
                        <View style={styles.userAddItemContainer}>
                            <Image style={styles.icon} source={require('../images/chat.png')} />
                            <Text style={styles.iconText}>{d.email}</Text>
                        </View>
                     </TouchableOpacity>
                     <TouchableOpacity onPress={this.showMapModal}>
                        <View style={styles.userAddItemContainer}>
                            <Image style={styles.icon} source={require('../images/location.png')}/>
                            <Text style={styles.iconText}>{d.address}</Text>
                        </View>
                     </TouchableOpacity>
                 </View>

                 <ActionSheet visible={this.state.actionsheet} onCancel={this.onCancel}>
                    <ActionSheet.Button onPress={this.onCapture}>Capture</ActionSheet.Button>
                    <ActionSheet.Button onPress={this.onSelectPhoto}>Photo</ActionSheet.Button>
                 </ActionSheet>
             </View>
         );
     }

     uploadImg(){
         this.setState({
             actionsheet: true
         });
     }

     onCancel(){
         this.setState({
             actionsheet: false
         })
     }

     onCapture(){
         const options = {
             mediaType: 'photo',
             videoQuality: 'medium'
         };

         ImagePickerManager.launchCamera(options, (res)=>{
             this.setState({
                 pic: res.uri,
                 actionsheet: false
             });
         });
     }

     onSelectPhoto(){
         const options = {
             mediaType: 'photo',
             videoQuality: 'medium'
         };

         ImagePickerManager.launchImageLibrary(options, (res) => {
             this.setState({
                 pic: res.uri,
                 actionsheet: false
             });
         });
     }

     goToMessage(){
         this.props.navigator.push({
             name: "serviceproviderinbox",
             title: "Service Provider Inbox"
         })
     }

     showModal(){
         this.setState({
            showModal: true
         });
     }

     closeModal(){
         this.setState({
             showModal: false
         });
     }
 }

 const styles = StyleSheet.create({
     userInfoContainer: {
         backgroundColor: '#f9dc7f',
         padding: 15,
         borderTopLeftRadius: 8,
         borderTopRightRadius:8,
         flexDirection: 'row'
     },

     userMainInfo: {
         width: 100,
         alignItems: 'center',
         justifyContent: 'center'
     },

     userImage: {
         width: 70,
         height: 70,
         borderRadius: 35
     },

     userName: {
         fontSize: 15,
         color: '#30629a',
         textAlign:'center',
         fontFamily:'Roboto-Medium',
         marginTop: 5
     },

     userAddInfo: {
         flex: 1
     },

     userAddItemContainer:{
         flexDirection: 'row',
         marginBottom: 5
     },

     icon: {
         width: 20,
         height: 20
     },

     iconText: {
         marginLeft: 10,
         fontFamily: 'Roboto-Medium',
         color: '#30639a',
         lineHeight: 20,
         fontSize: 12
     }
 });
