import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ListView,
    Image,
    TouchableOpacity
} from 'react-native';

import Communications from 'react-native-communications';

class ListItem  extends Component{
    constructor(props){
        super(props);

        this.goToMessage = this.goToMessage.bind(this);
    }

    render(){
        return (
            <View style={styles.listItemContainer}>
                <View style={styles.listItemTextContainer}>
                    <Text style={styles.font1}>John Johnson</Text>
                    <Text style={styles.font2}>Need an eye exam for drive license</Text>
                </View>
                <View style={styles.listItemTickIDContainer}>
                    <View style={styles.listItemTickIDWrapper}>                    
                        <Image source={require('../images/ticket.png')} style={styles.tickIcon} />
                        <Text style={styles.tickID}>
                            B85673
                        </Text>               
                    </View>
                    <View style={styles.mark}></View>     
                </View>
                <View style={styles.listItemIconContainer}>
                    <TouchableOpacity style={styles.telBtn} onPress={() => Communications.phonecall('012312321', true)}>
                        <Image source={require('../images/tel.png')} style={styles.telIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.msgBtn} onPress={this.goToMessage}>
                        <Image source={require('../images/message.png')} style={styles.msgIcon} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    goToMessage(){
        this.props.navigator.push({
            name: "serviceproviderinbox",
            title: "Service Provider Inbox"
        });
    }
}

export class ServiceProviderClientsPage extends Component{
    constructor(){
        super();

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 != r2
        });
        this.state = {
            dataSource: ds.cloneWithRows(
                [
                    'row1',
                    'row2'
                ]
            )
        }
    }

    render(){
        return (
            <View style={styles.container}>
                <Text style={[styles.headerText, styles.round]}>
                    Now
                </Text>
                <View style={styles.listItemContainer}>
                    <View style={styles.listItemTextContainer}>
                        <Text style={styles.font1}>John Johnson</Text>
                        <Text style={styles.font2}>Need an eye exam for drive license</Text>
                    </View>
                    <View style={styles.listItemTickIDContainer}>
                        <View style={styles.listItemTickIDWrapper}>                    
                            <Image source={require('../images/ticket.png')} style={styles.tickIcon} />
                            <Text style={styles.tickID}>
                                B85673
                            </Text>               
                        </View>
                        <View style={styles.mark}></View>     
                    </View>
                    <View style={styles.listItemIconContainer}>                        
                    </View>
                </View>
                <Text style={styles.headerText}>
                    Next
                </Text>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => <ListItem navigator={this.props.navigator}/>}
                    automaticallyAdjustContentInsets={false}                    
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        marginTop: 15,
        borderRadius: 8
    },

    headerText: {
        paddingTop: 10,
        paddingBottom: 10,
        color: '#30629a',
        backgroundColor: '#9fdfcb',
        paddingLeft: 15,
        fontSize: 18,
        fontFamily: 'Roboto-Medium',
        textAlign: 'center'
    },
    listItemContainer: {
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3',
        flexDirection: 'row'
    },
    font1: {
        fontSize: 14,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Thin'
    },
    font2: {
        fontSize: 12,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Thin'
    },
    listItemTextContainer: {
        flex: 1
    },
    listItemTickIDContainer: {       
        alignItems: 'center',
        justifyContent: 'center',       
        position: 'relative'
    },

    listItemTickIDWrapper: {
        margin: 5,
        flexDirection: 'row',
        backgroundColor: '#fadd7f',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        paddingLeft: 8,
        paddingRight: 8
    },  

    tickIcon: {
        width: 20,
        height: 20,
        margin:7
    },

    tickID: {
        lineHeight: 20,
        fontSize: 18,
        color: '#30629a',
        fontFamily: 'Roboto-Black'
    },

    mark: {
        position: 'absolute',
        width: 16,
        height: 16,
        top: 0,
        right: 0,
        backgroundColor: '#ff0000',
        borderRadius: 8
    },
    listItemIconContainer: {
        width: 20,
        marginLeft: 15
    }, 

    telBtn: {
        width: 20,
        height: 20
    },

    telIcon: {
        width: 20,
        height: 20
    },

    msgBtn: {
        width: 20,
        height: 20,
        marginTop: 5
    },

    msgIcon: {
        width: 20,
        height: 20
    },
    round: {
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8
    }    
});