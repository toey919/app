import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    ListView
} from 'react-native';

import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

//custom list item tag
class ListItem extends Component{

    //constructor
    constructor(props){
        super(props);
        this.onItemPress = this.onItemPress.bind(this);
    }

    //render
    render(){
        return (
            <TouchableOpacity onPress={this.onItemPress}>
                <View style={styles.listItemContainer}>
                    <View style={styles.listItemAvatarContainer}>
                        <Image style={styles.listItemAvatar} source={require('../images/user.png')} />
                    </View>
                    <View style={styles.listItemContent}>
                        <Text style={styles.listItemFont1}>{this.props.data.name}</Text>
                        <Text style={styles.listItemFont2}>{this.props.data.description}</Text>
                        <Text style={styles.listItemFont3}>{this.props.data.address}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    //onItemPress event
    onItemPress(){
        this.props.navigator.push({
            name: 'profile',
            title: 'Profile',
            id: this.props.data._id,
            type: this.props.data.type
        })
    }
}

//main page
export class MainPage  extends Component {

    //constructor
    constructor(props){
        super(props);
        this.state = {
            isVisible: false,
            tag: ""
        }

        this.onSearch = this.onSearch.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    //render
    render(){
        const query = gql`
            query SearchAllQuery($name: String!){
                searchAll(text: $name){
                    name,
                    description,
                    type,
                    address,
                    _id
                }
            }
        `;

        const ListViewContainer = (ret) => {
            console.log("main view list view data:");
            console.log(ret);
            if(ret.data.searchAll){
                const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
                const dataSource = ds.cloneWithRows(ret.data.searchAll);
                return (
                    <ListView
                        dataSource={dataSource}
                        renderRow={(rowData) => <ListItem data={rowData} navigator={this.props.navigator}/>}
                    />
                );
            }else{
                return null;
            }
        };

        const ListViewWithData = graphql(query, { options: { variables: {name: this.state.tag}}})(ListViewContainer);

        return (
            <View style={styles.container}>
                <View style={styles.searchContainer}>
                    <TextInput style={styles.searchInputContainer} underlineColorAndroid="#9fdfcb" placeholder="Olathe" onChangeText={(text) => this.onChange(text)} value={this.state.tag}/>
                    <TouchableOpacity style={styles.searchBtnContainer} onPress={this.onSearch}>
                        <Image style={styles.searchBtn} source={require('../images/search.png')} />
                    </TouchableOpacity>
                </View>
                {this.state.isVisible?
                <ListViewWithData/>
                :null}
            </View>
        );
    }

    onSearch(nav){
        if(this.state.tag.trim() != ""){
            this.setState({
                isVisible: true
            });
        }
    }

    onChange(text){
        this.setState({
            tag: text,
            isVisible: false
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        marginTop: 15,
        borderRadius: 8
    },

    searchContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#30629a'
    },

    searchInputContainer: {
        flex: 1,
        height: 50,
        backgroundColor: '#9fdfcb',
        fontFamily: 'Roboto-Light',
        color: '#f7f7f7',
        paddingLeft: 15,
        paddingRight: 15,
        fontSize: 15,
        borderTopLeftRadius: 8
    },

    searchBtnContainer: {
        width: 50,
        height: 50,
        backgroundColor: '#f4d782',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopRightRadius: 8
    },

    searchBtn: {
        width: 30,
        height: 30
    },

    listHeader:{
        backgroundColor: '#9fdfcb',
        color: '#30629a',
        fontFamily: 'Roboto-Medium',
        fontSize: 15,
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#30629a'
    },

    listItemContainer: {
        padding: 7,
        borderBottomWidth: 1,
        borderBottomColor: '#e3e3e3',
        flexDirection: 'row',
        flex: 1
    },

    listItemAvatarContainer: {
        width: 80,
        alignItems: 'center',
        justifyContent: 'center'
    },

    listItemAvatar: {
        width: 40,
        height: 40,
        borderRadius: 20

    },

    listItemContent: {
        flex: 1
    },

    listItemFont1: {
        fontSize: 18,
        color: '#505050',
        fontFamily: 'Roboto-Medium'
    },

    listItemFont2: {
        fontSize: 14,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Light'
    },

    listItemFont3: {
        fontSize: 13,
        color: '#cdcdcd',
        fontFamily: 'Roboto-Light'
}
    });
